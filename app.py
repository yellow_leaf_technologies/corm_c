from gevent.pywsgi import WSGIServer
from microservice_API import app
import optparse

parser = optparse.OptionParser()
parser.add_option('-i', '--ip', action="store", dest="IP",
                  help="IP address", default="0.0.0.0")
parser.add_option('-p', '--port', action="store", dest="PORT",
                  help="Port", default="5000")
parser.add_option('-x', '--proxy', action="store", dest="PROXY",
                  help="Proxy IP address with port", default=False)
parser.add_option('-d', '--debug', action="store", dest="DEBUG",
                  help="Debug mode on", default=False)
app_options, args = parser.parse_args()

if __name__ == '__main__':
    # Configuring and running Flask app
    app.config['proxy_address'] = app_options.PROXY
    app.run(host=app_options.IP, port=app_options.PORT, debug=app_options.DEBUG)

    print('IP:', app_options.IP)
    print('PORT:', app_options.PORT)
    print('PROXY:', app_options.PROXY)
    print('DEBUG:', app_options.DEBUG)

    # http server
    http_server = WSGIServer((app_options.IP, app_options.PORT), app)
    http_server.serve_forever()
