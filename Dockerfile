# Using Debian 
FROM debian:9

# Set the working directory
WORKDIR /app

# Copy the current directory
ADD . /app

# Installing packages from Debian repo and python pip
RUN apt-get update && apt-get install -y --no-install-recommends \
apt-utils \
libgtk2.0-dev \
xvfb \
chromium \
python3 \
python3-pip \
python3-setuptools && pip3 install wheel && pip3 install -r requirements.txt && \
apt-get remove -y python3-pip && \
rm -rf /var/lib/apt/lists/* && rm -rf /var/cache/apt/archives/*

# Outside port 5000
EXPOSE 5000

# Run microservice_API.py when the container launches
ENTRYPOINT ["python3", "app.py"]
