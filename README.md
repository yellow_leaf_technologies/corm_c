#### Building docker image
##### Build from Dockerfile
Go to the project directory where Dockerfile located and run the following command

```
 docker build -t apiflask .
```
Then simply run 
```
docker run -p 5000:5000 apiflask
docker run -p 5000:5000 apiflask --help
```
#### Run docker image with script options
Output of --help option
```
Usage: app.py [options]

Options:
  -h, --help            show this help message and exit
  -i IP, --ip=IP        IP address
  -p PORT, --port=PORT  Port
  -x PROXY, --proxy=PROXY
                        Proxy IP address with port
  -d DEBUG, --debug=DEBUG
                        Debug mode on
```
##### Example
```
docker run -p 5000:5000 apiflask --port 5004 --proxy 38.29.152.9:53281 --debug True
docker run -p 5000:5000 apiflask --ip 10.10.10.2 --port 5004

```
#### Loading docker image
##### Download image file from repository
Link to Image https://gitlab.com/yls/corm_c/blob/master/apiflask.tar. After donwload 
```
docker load < apiflask.tar
```
#### Running docker image
##### Run docker image  
```
docker run -p 5000:5000 apiflask 
```
Default address is `0.0.0.0:5000`
Get list of args
```
docker run -p 5000:5000 apiflask --help

```
##### URL and Method
Send form data using Post method. 
```
http://0.0.0.0:5000/get_json
```
#### JSON example
```
{
 "first_name":"Robert",
 "last_name":"Wensley",
 "day_of_birth":"25/01/2000",
 "street":"5756 Amelia Springs Circle",
 "city":"Haymarket",
 "state":"VA",
 "zip_code":"20169",
 "tel":"650-656-5288"
}
```
