from skimage.measure import compare_ssim
import numpy as np
import cv2
import urllib
import time


def captcha(driver):
    my_result = ''

    ###############################
    #   BLOCK FOR SOLVE CAPTCHA   #
    ###############################
    while len(my_result) != 8:

        img = driver.find_elements_by_tag_name('img')[9]
        src = img.get_attribute('src')
        urllib.request.urlretrieve(src, "captcha.jpeg")
        time.sleep(2)

        chars = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        template = []
        for i in chars:
            thresh_img = cv2.imread('chars/' + i + '.png')

            template.append(thresh_img[:, :, 1])

        filename = "captcha.jpeg"

        img = cv2.imread(filename)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        _, mask = cv2.threshold(gray, 150, 255, cv2.THRESH_BINARY)
        _, cnts, _ = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        symbols = []
        sorted_cnts = sorted(cnts, key=lambda ctr: cv2.boundingRect(ctr)[0])

        for idx in range(len(sorted_cnts)):
            mask_to_crop = np.zeros_like(mask)
            cv2.drawContours(mask_to_crop, sorted_cnts, idx, 255, -1)
            out = np.zeros_like(mask)
            out[mask_to_crop == 255] = mask[mask_to_crop == 255]

            (x, y) = np.where(mask_to_crop == 255)
            (topx, topy) = (np.min(x), np.min(y))
            (bottomx, bottomy) = (np.max(x), np.max(y))
            symbols.append(out[topx:bottomx + 1, topy:bottomy + 1])
        print('Letter solved(should be 8):', len(cnts))
        scores = []
        if len(cnts) == 8:
            for symbol in symbols:
                for i in template:
                    resized = cv2.resize(i, (symbol.shape[1], symbol.shape[0]))
                    score = compare_ssim(symbol, resized, full=False)
                    scores.append(score)

                my_result += chars[scores.index(max(scores))]
                scores.clear()
        else:
            driver.refresh()
    return my_result
