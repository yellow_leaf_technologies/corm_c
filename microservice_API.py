from flask import Flask, request, abort, jsonify
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from pyvirtualdisplay import Display
from datetime import datetime
import calendar
import time
from ylt_captcha_solver import captcha

app = Flask(__name__)


@app.route('/get_json', methods=['POST'])
def get_json():
    if not request.json:
        abort(400)

    data = request.get_json()
    url = 'https://www.optoutprescreen.com'
    result = run_bot(data, url)

    return result


def run_bot(data, url):
    try:
        # Setting data to input
        first_name = data['first_name']
        # middle_name = data['middle_name']
        last_name = data['last_name']
        day_of_birth = data['day_of_birth']
        date = datetime.strptime(day_of_birth, '%m/%d/%Y')
        day = str(date.day)
        month = calendar.month_name[date.month]
        year = str(date.year)
        street = data['street']
        city = data['city']
        state = data['state']
        zip_code = data['zip_code']
        tel = data['tel']

        # Chrome driver and its options in headless mode
        driver_options = webdriver.ChromeOptions()
        # Checking proxy
        if app.config.get('proxy_address'):
            driver_options.add_argument('--proxy-server=%s' % app.config.get('proxy_address'))
        driver_options.add_argument('--headless')
        driver_options.add_argument("--no-sandbox")
        driver_options.add_argument("--disable-gpu")
        # driver_options.add_argument("--incognito")
        # driver_options.add_argument("--remote-debugin-port=9222")
        driver_options.add_argument("--screen-size=1200x800")
        driver = webdriver.Chrome(executable_path='./chromedriver', chrome_options=driver_options)
        driver.get(url)

        # Selenium
        search_button = driver.find_element_by_name('newform').click()
        time.sleep(2)

        search_text = driver.find_element_by_xpath("//input[@name='optChoice'][@type='radio'][@value='1']").click()
        search_submit = driver.find_element_by_name('submit').click()
        captcha_solve = captcha(driver)

        search_captcha = driver.find_element_by_name('guess').send_keys(captcha_solve)
        search_fname = driver.find_element_by_name('fname').send_keys(first_name)
        # search_mname = driver.find_element_by_name('mname').send_keys(middle_name)
        search_lname = driver.find_element_by_name('lname').send_keys(last_name)

        search_month = Select(driver.find_element_by_name('dob_month'))
        search_month_result = search_month.select_by_visible_text(month)
        search_day = Select(driver.find_element_by_name('dob_day'))
        search_day_result = search_day.select_by_visible_text(day)
        search_year = driver.find_element_by_name('dob_year').send_keys(year)

        search_street = driver.find_element_by_name('address1').send_keys(street)
        search_city = driver.find_element_by_name('city').send_keys(city)

        search_state = Select(driver.find_element_by_name('state'))
        search_state_result = search_state.select_by_visible_text(state)
        search_zipcode = driver.find_element_by_name('zipcode').send_keys(zip_code)
        search_telephone = driver.find_element_by_name('telephone').send_keys(tel)

        submit = driver.find_element_by_name('submitBack').click()
        endSession = driver.find_element_by_name('endsession').click()
        driver.quit()
        return jsonify({'first_name': data['first_name'],
                        'last_name': data['last_name'], 'registered': True, 'error_msg': False})
    except Exception as e:
        return jsonify({'first_name': data['first_name'],
                        'last_name': data['last_name'], 'registered': False, 'error_msg': str(e)})

